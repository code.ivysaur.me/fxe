# fxe

![](https://img.shields.io/badge/written%20in-PHP%2C%20Javascript%2C%20Python-blue)

A formula parser written identically in multiple languages.

FXE (Formula eXpression Evaluator) parses formulas using intuitive excel-like syntax. It's safe to pass untrusted input to.

- Respects BEDMAS order of operations
- Functions: IF(), MIN(), MAX(), OR(), AND() and easy to add more
- Division by zero produces zero

FXE is implemented near-identically in several languages to allow parsing the same user input across a range of systems. All implementations pass the same test cases.

Tags: PL


## Download

- [⬇️ fxe_r15.zip](dist-archive/fxe_r15.zip) *(11.28 KiB)*
- [⬇️ fxe_r14.zip](dist-archive/fxe_r14.zip) *(11.28 KiB)*
